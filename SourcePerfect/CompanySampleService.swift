//
//  CompanySampleService.swift
//  SwiftEx
//
//  Created by Volkov Alexander on 07.11.2019.
//

import Foundation

public class CompanySample: CrudObject {
    
    public var id: IdType!
    var name = ""
}

open class CompanySampleService {
    
    // database service
    public var service = CrudServiceMySQL<CompanySample>()
    
    public static func sample() {
        _ = try? DatabaseUtil.shared.database.create(CompanySample.self, primaryKey: \.id, policy: .reconcileTable)
        
        let object = CompanySample()
        object.name = "name1"
        let service = CompanySampleService()
        _ = try? service.create(object)
        print(service.getAll())
    }
    
    public func getAll() -> [CompanySample] {
        do {
            return try service.selectAll()
        }
        catch let error {
            print("ERROR \(error)")
            return []
        }
    }
    
    public func create(_ object: CompanySample) throws -> CompanySample{
        object.id = Int.rand()
        return try service.create(object)
    }
}
