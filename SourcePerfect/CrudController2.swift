//
//  CrudController2.swift
//  SampleServer
//
//  Created by Volkov Alexander on 02.11.2019.
//


import Foundation
import PerfectHTTP
import PerfectLib
import SwiftExApi

// Controller that has DB and API object difference.
// API object (T: CrudObject) must implement DbConvertable protocol
// ```
// routes.addCRUD(uri: "/location", handler: CrudController2<LocationApiModel, LocationModel>().handler)
// ```
/*
 1. Create API Model (:CrudObject) and DB model (:DbObject).
 2. Add handler as mentioned above.
 3. Customize `postValidation` and `backConvertion`. Without `backConvertion` API will return DB objects.
 4. API models are used for bodies (POST, PUT) and in result if you manually convert DB to API in `backConvertion`.
 */
open class CrudController2<T: CrudObject, D: DbObject> {
    
    // optional: validation of created DB object. Used for POST, PUT
    public var postValidation: ((T,D, HTTPRequest) throws ->())?
    
    // the back convertion from DB to API. Used for POST, GET, PUT
    public var backConvertion: ((T?, D) throws ->(T))?
    
    // the main controller for DB objects
    public var controller: CrudController<D>
    
    /// Initializer
    ///
    /// - Parameter postValidation: the callback used to validate DB object created from API object before `service.create`
    public init(controller: CrudController<D> = CrudController<D>(),
                postValidation: ((T,D, HTTPRequest) throws ->())? = nil,
                backConvertion: ((T?, D) throws ->(T))? = nil) {
        self.controller = controller
        self.postValidation = postValidation
        self.backConvertion = backConvertion
    }
    
    // Overrides POST and PUT methods
    public func handler(request: HTTPRequest, response: HTTPResponse) throws {
        switch request.method {
        // GET handler
        case .get:
            let items = try controller.service.selectAll()
            if let callback = backConvertion {
                var list = [T]()
                for item in items {
                    list.append(try callback(nil, item))
                }
                
                // dodo wrap into page
                // dodo pagination
                try response.response(list)
            }
            else {
                try response.response(items)
            }
        // POST handler
        case .post:
            let object: T = try request.body(); try controller.validatePost(object)
            
            // Convert to database object
            let dbObject: D = try (object as! DbConvertable).toDb() as! D
            try controller.validatePost(object)
            
            // Extra validation, e.g. check existence of inner objects
            try self.postValidate(object: object, dbObject: dbObject, request: request)
            
            // Logic
            let newObject = try controller.service.create(dbObject)
            
            // Convert back to API
            var newApiObject: T = object
            if let callback = backConvertion {
                newApiObject = try callback(object, newObject)
            }
            else { // just update ID
                newApiObject.id = newObject.id
            }
            
            // Response
            try response.response(newApiObject)
            response.status = .created
            
        // PUT handler
        case .put:
            let object: T = try request.body(); try controller.validatePut(object)
            
            /// Reset ID to the ID provided in the URL
            let id = try request.getId()
            var o = object
            o.id = id
            _ = try controller.service.select(byId: id)
            
            // Convert to database object
            let dbObject: D = try (o as! DbConvertable).toDb() as! D
            try controller.validatePut(object)
            
            // Extra validation, e.g. check existence of inner objects
            try self.postValidate(object: object, dbObject: dbObject, request: request)
            
            // Logic
            try controller.service.update(dbObject)
            
            // Convert back to API
            var newApiObject: T = o
            if let callback = backConvertion {
                newApiObject = try callback(object, dbObject)
            }
            
            // Response
            try response.response(newApiObject)
        default:
            return try controller.handler(request: request, response: response)
        }
        response.completed()
    }
    
    // Validate object using `postValidation` callback and wrap any exception into 404
    public func postValidate(object: T, dbObject: D, request: HTTPRequest) throws ->() {
        do {
            try self.postValidation?(object, dbObject, request)
        } catch let error {
            if let httpError = error as? HTTPResponseError, case HTTPResponseStatus.notFound = httpError.status {
                throw HTTPResponseError(status: .badRequest, description: "Incorrect inner object: \(httpError)")
            }
            throw error
        }
    }
}
