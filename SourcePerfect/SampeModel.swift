//
//  LocationApi.swift
//  SwiftEx
//
//  Created by Volkov Alexander on 02.11.2019.
//

import Foundation
import SwiftEx
/*
 This file provides an example of related model objects
 1. For first launch use the following command to create a table.
 try Service.shared.database.create(LocationModel.self, primaryKey: \.id, policy: .reconcileTable)
 2. Connect to MySQL and copy `show create table LocationModel;` output, add AUTO_INCREMENT and put into initial .sql.
 3. Comment or remove `Service.shared.database.create..` line.
 4. Relaunch database and server (server must be restarted after database is restarted).
 
 // Example handlers
 let countryController = CrudController<CountryModel>()
 routes.addCRUD(uri: "/country", handler: countryController.handler)
 let locationController = CrudController2<LocationApiModel, LocationModel>()
 locationController.postValidation = { object, dbObject, request in
    // try to ge country. It will throw 404 if no such country (will be wrapped to 400)
    let _: CountryModel = try countryController.service.select(byId: object.country.id)
 }
 locationController.backConvertion = { (o: LocationApiModel?, db: LocationModel) -> LocationApiModel in
    var model: LocationApiModel = try (o ?? db.toDb() as! LocationApiModel)
    model.id = db.id
    let c: CountryModel = try countryController.service.select(byId: model.country.id)
    model.country = c
    return model
 }
 routes.addCRUD(uri: "/location", handler: locationController.handler)
 */
// Country model
public struct CountryModel: CrudObject, PostValidate {
    public var id: IdType!
    public var name: String!
    
    // validate POST requests for this object
    public func validatePost() throws {
        // dodo String.validString(), String.isString
        try String.validString(name, "Country.name")
        // dodo Int.isId, Int.validId, String.validId
//        if id == nil || id <= 0 { throw "Country.id" }
    }
    
    // validates inner childs of other objects, e.g. Location.county
    public func validateChild() throws {
        try Int.isId(id, "Country.id")
    }
}

extension String {
    
    // Validate string to be not nil and not empty
    public static func validString(_ str: String?, _ fieldName: String? = nil) throws {
        if str == nil || str!.trim().isEmpty { throw fieldName ?? "<one of them>" }
    }
    
    public static func validStrings(_ strings: String?...) throws {
        for str in strings {
            if str == nil || str!.trim().isEmpty { throw "<one of them>" }
        }
    }
}

extension Int {
    
    // Check if valud ID
    public static func isId(_ id: Int?, _ fieldName: String? = nil) throws {
        if id == nil || id! <= 0 { throw fieldName ?? "<one of them>" }
    }
    
    // Check if valud ID
    public static func isId(_ id: String?, _ fieldName: String? = nil) throws {
        if id == nil || Int(id!) == nil || Int(id!)! <= 0 { throw fieldName ?? "<one of them>" }
    }
    
    public static func rand() -> String {
        let i: Int = Int.rand()
        return "\(i)"
    }
}

// Location model for API
// POST validation
public struct LocationApiModel: CrudObject,
                    PostValidate, PutValidate,
                    DbConvertable {
    
    public var id: IdType!
    public var name = ""
    public var phone: String?
    public var country: CountryModel!
    
    // :PostValidate
    public func validatePost() throws {
        if name.isEmpty { throw "name" }
        if country == nil { throw "country" }
        try country?.validateChild()
    }
    
    // :PutValidate
    public func validatePut() throws { try validatePost() }
    
    // :DbConvertable
    public func toDb() throws -> Any {
        let source = self
        
        // Convert from LocationAPI (the required fields must be defined with '!' because initially will be nil
        var object: LocationModel = try source.convertTry()
        
        // Customize fields
        object.countryId = source.country?.id
        return object
    }
}

// Location model for database
struct LocationModel: DbObject, DbConvertable {
    var id: IdType!
    var name = ""
    var phone: String?
    var countryId: IdType!
    
//    enum CodingKeys: String, CodingKey {
//        case name, phone, countryId
//    }
    
    // :DbConvertable
    public func toDb() throws -> Any {
        let source = self
        
        // Convert from LocationAPI (the required fields must be defined with '!' because initially will be nil
        var object: LocationApiModel = try source.convertTry()
        
        // Customize fields
        if let countryId = countryId {
            object.country = CountryModel(id: countryId, name: "")
        }
        return object
    }
}

//////////////////////////////// DELETE

extension LocationModel {
    
    // DELETE - it's not complete and no way to convert CodingKeys to WritableKeyPath
    static func fromGeneral<T: Codable>(_ object: T) throws -> LocationModel {
//        let data = try JSONEncoder().encode(object)
        // Decode with basic fields
        let decoder: Decoder = JSONDecoder() as! Decoder
        let container = try decoder.container(keyedBy: LocationModel.CodingKeys.self)
        var object = LocationModel()
//        for key in container.allKeys {
            let value = try container.decode(String.self, forKey: LocationModel.CodingKeys.name)
            let k: WritableKeyPath<LocationModel, String> = \LocationModel.name
            object[keyPath: k] = value
//        }
        return object
    }
}

