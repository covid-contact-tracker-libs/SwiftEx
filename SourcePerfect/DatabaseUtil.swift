//
//  DatabaseUtil.swift
//  SwiftEx
//
//  Created by Volkov Alexander on 02.11.2019.
//

import Foundation
import PerfectHTTP
import PerfectCRUD
import PerfectMySQL

// The class used to provide database initialization
open class DatabaseInit {
    public static var callback: ((Database<MySQLDatabaseConfiguration>)->())?
}

// Database utility to get reference to the database instance
// `DatabaseUtil.shared.database`
open class DatabaseUtil {
    
    /// the database reference
    public var database: Database<MySQLDatabaseConfiguration>!
    
    // shared instance
    public static let shared = DatabaseUtil()
    
    // Initializer. Creates connection.
    init() {
        do {
            let db = Database(configuration: try MySQLDatabaseConfiguration(
                database: Configuration.databaseName,
                host: Configuration.databaseHost,
                port: Configuration.databasePort,
                username: Configuration.databaseUser,
                password: Configuration.databasePassword))
            // Create the table if it hasn't been done already.
            //try DatabaseUtil.shared.database.create(Model.self, primaryKey: \.id, policy: .reconcileTable)
//            DatabaseInit.callback = { db in
//                do {
//                    try db.create(Model.self, primaryKey: \.id, policy: .reconcileTable)
//                }
//                catch let error {
//                    print("ERROR: \(error)")
//                }
//            }
            self.database = db
            DatabaseInit.callback?(db)
        }
        catch let error {
            print("ERROR: \(error)")
        }
    }

}
