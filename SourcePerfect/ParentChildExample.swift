//
//  ParentChildExample.swift
//  SwiftEx
//
//  Created by Volkov Alexander on 08.11.2019.
//

import Foundation
import PerfectCRUD

struct Parent: Codable {
    let id: Int
    var children: [Child]!
}

struct Child: Codable {
    let id: Int
    let name: String
    let parentId: Int
}

// Creates Parent and Child tables
// Tries to select objects by joining tables
func sampleParentChild1() throws {
    let db = DatabaseUtil.shared.database!
    
    // Create tables and insert some data
    try db.transaction {
        do {
            let table1 = try db.create(Parent.self, policy: [.dropTable, .shallow])
            try table1.insert(Parent(id: 1, children: nil))
            let table2 = try db.create(Child.self, policy: [.shallow, .dropTable])
            try table2.insert([
                Child(id: 1, name: "John", parentId: 1),
                Child(id: 2, name: "Sara", parentId: 1),
                Child(id: 3, name: "Remi", parentId: 1)])
        }
        catch let error {
            print(error)
        }
    }
    
    // Join
    let join = try db.table(Parent.self)
        .join(\.children,
              on: \.id,
              equals: \.parentId)
        .where(\Parent.id == 1)
    
    guard let parent = try join.first() else { fatalError("Failed to find parent id: 1") }
    guard let children = parent.children else { fatalError("Parent had no children") }
    
    print("parent: \(parent)")
    print("children: \(children)")
    
}

// Uses SQL syntax to query parent and child
func sampleParentChild2() throws {
    let sql1 = "select Parent.* from Parent inner join Child where Parent.id = Child.parentId"
    let sql2 = "select Child.* from Parent inner join Child where Parent.id = Child.parentId"
    let db = DatabaseUtil.shared.database!
    let parents = try db.sql(sql1, bindings: [], Parent.self)
    let children = try db.sql(sql2, bindings: [], Child.self)
    
    for c in parents { print(c) }
    for c in children { print(c) }
    
    var map = [Int: Parent]()
    for i in 0..<parents.count {
        var p = parents[i]
        if map[p.id] == nil {
            map[p.id] = p
        }
        p = map[p.id]!
        let c = children[i]
        if p.children == nil {
            p.children = [Child]()
        }
        p.children.append(c)
        map[p.id] = p
    }
    let list: [Parent] = Array(map.values)
    print(list)
}
