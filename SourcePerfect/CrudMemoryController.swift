//
//  CrudMemoryController.swift
//  SampleServer
//
//  Created by Volkov Alexander on 31.10.2019.
//

import Foundation
import PerfectHTTP
import PerfectLib
import SwiftExApi

/**
 In-memory storage-based controller.
 routes.addCRUD(uri: "/coordinate", handler: CrudMemoryController<Coordinate>().handler)
 */
open class CrudMemoryController<T: CrudObject>: CrudController<T> {
    
    private var items = [T]()
    
    public override init() {
    }
    
    /// CRUD handler for Model object
    public override func handler(request: HTTPRequest, response: HTTPResponse) throws {
        switch request.method {
            
        // GET handler
        case .get:
            try response.response(items)
            
        // POST handler
        case .post:
//            guard let body = request.json() else { response.completed(status: .badRequest, message: "Not JSON body"); return }
//            guard var object: T = try? body.decodeIt() else { response.completed(status: .badRequest, message: "Wrong body format"); return } // this does not throw errors and wraps into "Wrong body format" message
            var object: T = try request.body()
            try validatePost(object)
            
            object.id = Int.rand()
            items.append(object)
            try response.response(object)
            response.status = .created
            
        // PUT handler
        case .put:
            let object: T = try request.body()
            try validatePut(object)
            
            /// Reset ID to the ID provided in the URL
            let id = try request.getId()
            var o = object
            o.id = id
            
            // Find if exists
            let (_, index) = try select(byId: id)
            // Update
            items.remove(at: index)
            items.insert(o, at: index)
            
            try response.response(o)
            
        // DELETE handler
        case .delete:
            let id = try request.getId()
            let (_, index) = try select(byId: id)
            
            items.remove(at: index)
            
        // Other methods handler
        default:
            throw HTTPResponseError(status: .methodNotAllowed, description: "Method is not supported")
        }
        response.completed()
    }
    
    private func select(byId id: IdType) throws -> (T,Int) {
        var found: Int?
        for i in 0..<items.count {
            if items[i].id == id {
                found = i; break
            }
        }
        guard let index = found else { throw HTTPResponseError(status: .notFound, description: "Object not found")}
        return (items[index], index)
    }
}
