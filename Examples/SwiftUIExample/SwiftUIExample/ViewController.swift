//
//  ViewController.swift
//  SwiftUIExample
//
//  Created by Volkov Alexander on 26.10.2019.
//  Copyright © 2019 Alexander Volkov. All rights reserved.
//

import UIKit
import SwiftEx

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        ActivityIndicator(parentView: self.view).start()
    }
}

