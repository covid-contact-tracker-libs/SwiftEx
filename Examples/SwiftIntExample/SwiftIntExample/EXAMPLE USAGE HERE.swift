//
//  ViewController.swift
//  SwiftIntExample
//
//  Created by Volkov Alexander on 26.10.2019.
//  Copyright © 2019 Alexander Volkov. All rights reserved.
//

import UIKit
import SwiftEx

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        validateRestServiceApi()
    }

    
    func validateRestServiceApi() {
        RestServiceApi.callback401 = {
            print("ERROR: User is not authorized")
        }
//        RestServiceApi.get(url: "http://api.plos.org/search?q=title:DNA")
//            .call(on: self, { (json: SampleJson) in
//                print(json.response?.numFound)
//            })
    }

}


class SampleJson: Codable {
    var response: SampleJsonResponse?
}
class SampleJsonResponse: Codable {
    var numFound: Int?
}
