// swift-tools-version:4.1

import PackageDescription

let package = Package(
    name: "SwiftEx",
    products: [
        .library(name: "SwiftEx", targets: ["SwiftEx"]),
        .library(name: "SwiftExData", targets: ["SwiftExData"]),
        .library(name: "SwiftExUI", targets: ["SwiftExUI"]),
        .library(name: "SwiftExInt", targets: ["SwiftExInt"]),
        .library(name: "SwiftExApi", targets: ["SwiftExApi"]),
        .library(name: "SwiftExPerfect", targets: ["SwiftExPerfect"])
    ],
    dependencies: [
        .package(url: "https://github.com/SwiftyJSON/SwiftyJSON.git", from: "4.2.0"),
        .package(url: "https://github.com/ReactiveX/RxSwift.git", from: "4.4.2"),
        .package(url: "https://github.com/RxSwiftCommunity/RxAlamofire.git",
                 from: "4.3.0"),
        .package(url: "https://github.com/Alamofire/Alamofire.git", from: "4.8.1"),
        .package(url: "https://github.com/PerfectlySoft/Perfect-HTTPServer.git", from: "3.0.0"),
        .package(url: "https://github.com/PerfectlySoft/Perfect-MySQL.git", from: "3.0.0")
    ],
    targets: [
        .target(name: "SwiftEx", path: "Source"),
        .target(name: "SwiftExData", dependencies: ["SwiftEx", "SwiftyJSON"], path: "SourceData"),
        .target(name: "SwiftExUI", dependencies: ["SwiftEx", "SwiftExData"], path: "SourceUI"),
        .target(name: "SwiftExApi", dependencies: ["SwiftEx", "SwiftExData"], path: "SourceApi"),
        .target(name: "SwiftExPerfect", dependencies: ["SwiftEx", "SwiftExData", "SwiftExApi", "PerfectMySQL", "PerfectHTTPServer"], path: "SourcePerfect"),
        .target(name: "SwiftExInt", dependencies: ["SwiftEx", "SwiftExData", "SwiftExUI", "SwiftExApi", "Alamofire", "RxSwift", "RxAlamofire"], path: "SourceInt")
    ])
